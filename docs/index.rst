.. pop-example-poetry documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pop-example-poetry's Documentation!
==============================================


.. toctree::
   :maxdepth: 2
   :glob:

   topics/pop-example-poetry
   tutorial/index
   releases/index

.. toctree::
   :caption: Get Involved
   :maxdepth: 2
   :glob:

   topics/contributing
   topics/license
   Project Repository <https://gitlab.com/saltstack/pop/pop-create/>

Indices and tables
==================

* :ref:`modindex`
