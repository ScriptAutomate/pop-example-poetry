#!/usr/bin/env python3
import pop.hub


def start():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="pop_example_poetry")
    hub["pop_example_poetry"].init.cli()
