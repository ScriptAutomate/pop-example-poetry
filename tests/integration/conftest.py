from unittest import mock

import pytest


@pytest.fixture(scope="session")
def hub(hub):
    for dyne in ["pop_example_poetry"]:
        hub.pop.sub.add(dyne_name=dyne)

    with mock.patch("sys.argv", ["pop-example-poetry"]):
        hub.pop.config.load(
            ["pop_example_poetry"],
            cli="pop_example_poetry",
            parse_cli=True,
        )

    yield hub
