from unittest import mock


def test_cli(hub):
    with mock.patch("sys.argv", ["pop-example-poetry"]):
        hub.pop_example_poetry.init.cli()
