def test_cli(mock_hub, hub):
    mock_hub.pop_example_poetry.init.cli = hub.pop_example_poetry.init.cli
    mock_hub.pop_example_poetry.init.cli()
    mock_hub.pop.config.load.assert_called_once_with(
        ["pop_example_poetry"], "pop_example_poetry"
    )
